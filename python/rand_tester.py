import json
import socket
import sys
from random import randint

CONS_THROTTLE = 0.3

SWITCH_LANE = True

LINES = [0, 0, 0]

class NoobBot(object):
    throttle_value=0.5
    max_speed = 0
    last_pos = 0

    pieces = []
    tick = 0

    kill = False
    lastpos = 0
    lastswpos=0

    def __init__(self, socket, name, key):
        self.enougth = 0
        self.socket = socket
        self.name = name
        self.key = key

    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.send(msg + "\n")

    def on_game_init(self,data):
        self.pieces = data['race']['track']['pieces']
        print data['race']['track']['lanes']

    def join(self):
        return self.msg("join", {"name": self.name,
                                 "key": self.key})

    def throttle(self, throttle):
        self.msg("throttle", throttle)

    def ping(self):
        self.msg("ping", {})

    def run(self):
        self.join()
        self.msg_loop()

    def on_join(self, data):
        # print("Joined")
        self.ping()

    def on_game_start(self, data):
        # print("Race started")
        self.done = True
        # if SWITCH_LANE:
        self.ping()
        # else:
        #     self.msg("switchLane", "Right")

    def random_throttle(self):
        self.throttle_value =float(randint(6550,6568))/10000
        # print "throttle:%f"%self.throttle_value
        self.throttle(self.throttle_value)

    def on_car_positions(self, data):
        car_pos_index = data[0]['piecePosition']['pieceIndex']  # hardcoded car 0 !!!!!
        car_pos = data[0]['piecePosition']['inPieceDistance']  # hardcoded car 0 !!!!!
        car_angle = data[0]['angle']  # hardcoded car 0 !!!!!
        self.lane = data[0]['piecePosition']['lane']['endLaneIndex']  # hardcoded car 0 !!!!!
        print "%d.%d\r" % (car_pos_index, car_pos),

        if 'switch' in self.pieces[car_pos_index+1]:
            if self.done:
                des_lane = LINES.pop(0)
                print self.lane, des_lane
                if self.lane < des_lane:
                    self.msg("switchLane", "Right")
                elif self.lane > des_lane:
                    self.msg("switchLane", "Left")
                print self.pieces[car_pos_index+1]
                self.done=False
                self.enougth=0
        elif 'switch' in self.pieces[car_pos_index]:
            self.lastswpos=car_pos
            print self.tick, "\t", car_pos_index, "\t", car_pos , "\t", self.lane
        elif 'switch' in self.pieces[car_pos_index-1]:
            # if self.lastpos == 0:
            #     self.lastpos=car_pos
            if self.enougth < 5:
                print self.tick, "\t", car_pos_index, "\t", car_pos, "\t", self.lane
            self.enougth += 1
            # else:
            #     print SWITCH_LANE, "\t", self.tick, "\t", car_pos_index, "\t", car_pos-self.lastpos, "\t", self.lastswpos, "\t", self.lastpos, "\t", car_angle
            #     print
            #     self.kill = True
            self.done = True

        self.throttle(CONS_THROTTLE)
        # car_pos = data[0]['piecePosition']['inPieceDistance']  # hardcoded car 0 !!!!!
        # speed = car_pos - self.last_pos
        # self.last_pos = car_pos
        # if speed > self.max_speed:
        #     self.max_speed = speed
        #     print speed

    def on_crash(self, data):
        print("Someone crashed")
        self.kill=True
        self.ping()

    def on_game_end(self, data):
        print("Race ended")
        self.ping()

    def lap(self, data):
        print("lap time: %d (speed: %f)"%(data['lapTime']['millis'],self.throttle_value))
        self.random_throttle()
        # self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
            'lapFinished': self.lap,
            'gameInit': self.on_game_init,
        }
        socket_file = s.makefile()
        line = socket_file.readline()
        while line:
            if self.kill:
                break
            msg = json.loads(line)
            if 'gameTick' in msg:
                self.tick = msg['gameTick']

            msg_type, data = msg['msgType'], msg['data']
            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                # print("Got {0}".format(msg_type))
                self.ping()
            line = socket_file.readline()


if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        while True:
            s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            s.connect((host, int(port)))
            bot = NoobBot(s, name, key)
            bot.run()

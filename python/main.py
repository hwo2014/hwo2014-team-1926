import json
import socket
import sys
from random import randint
from math import pi, sqrt
import datetime
import TrackInfo


class STATE:
    NORMAL, TURBO, BRAKE = range(3)


KILL_ON_CRASH = False

CONS_MAX_THROTTLE = 1.00
BRAKE_POWER = 0.0

MAX_SPEED = 100
LOW_CURVE_RAD = 90
LOW_SPEED = 6.3
ECLIPSE_INDEX = 0.5

AFTER_TURBO_DEGRADE = 0.975
MOTOR_POWER = 5

#ACCEL PARAMS
# kemloid power/5 - speed*0.02
ACCELL_DIVIDER = 5
TRACTION = 0.02

RESPONSE_DISTANCE = 3

RADIUS_STRAIGHT_CONST = 0

CURVE_BOOST_LIMIT = 20  # 1. boost if angle under
ANGULAR_BOOST_BOUND = 4

ANGULAR_BOOST_LIMIT = 0.1  # 2. boost if av under
CURVE_BOOST_BOUND = 55

ANGULAR_MAX = 5
CURVE_BOOST = 0.1

CURVE_BRAKE_LIMIT = 47  # save me brake
ANGULAR_BRAKE_LIMIT = 1.5
CURVE_BRAKE = 0.5

EMERGENCY_BRAKE_BOUND = 58
EMERGENCY_BRAKE_ANGULAR = 0.8

# CURVE_BOOST_LIMIT = 20  # 1. boost if angle under
# ANGULAR_BOOST_BOUND = 4
#
# ANGULAR_BOOST_LIMIT = 0.1  # 2. boost if av under
# CURVE_BOOST_BOUND = 55
#
# ANGULAR_MAX = 5
# CURVE_BOOST = 0.1
#
# CURVE_BRAKE_LIMIT = 40  # save me brake
# ANGULAR_BRAKE_LIMIT = 1.8
# CURVE_BRAKE = 1
#
# EMERGENCY_BRAKE_BOUND = 58
# EMERGENCY_BRAKE_ANGULAR = 0.8


#  !!!!!!!!!!! CONST PHYSICS !!!!!!!!!!!!
# CURVE_TESTING = {
#     40: 4.65,              # ger
#     60: 5.61,             # ger
#     90: 6.45,
#     110: 7.1,  # 7.108,
#     # 110: 7.0,
#     180: 9.1,
#     190: 9.3,  # too short to test
#     200: 9.5,
#     210: 11,  # too short to test
#     220: 12
# }

CURVE_TESTING = {
    40: 4.20,  # 56,  # 30,             # ger fr ger
    60: 5.50,  # 56,  # 54,             # ger fr ger
    90: 6.35,  # 40,  # 38,             # fr
    110: 6.9,  # 7.1,
    180: 8.5,   # 8.9                   # usa # fr
    190: 8.6,                           # fr
    200: 8.7,   # 8.9                   # usa   # could probably use more
    210: 10,      # 10.5,               # fr
    220: 12
}

magic_x_value = 0.98
magic_c_value = 0.0

USE_FUZZOID_POSTPROCERSSING = True
VERBOUSE_FUZZOID = True
# USE_FUZZOID_POSTPROCERSSING = False
# VERBOUSE_FUZZOID = False

log = None
# log = open("c:/Dropbox/Public/log.csv", "w+")


tick = -1

DEGRADED = 'degraded'
CURVE = 'curve'

# TODO WTF srsly?
# OPTTRACK = [   1, 0, 0, 1, 1, 1,      # kelmoid
#             1, 1, 0, 0, 1, 1, 1,
#             1, 1, 0, 0, 1, 1, 1,
#             1, 1, 0, 0, 1, 1, 1, 1]
#
# OPTTRACK = [   0, 1, 1, 0, 1, 1,        # german
#             1, 0, 1, 1, 0, 1, 1,
#             1, 0, 1, 1, 0, 1, 1,
#             1, 0, 1, 1, 0, 1, 1, 1]

# OPTTRACK = [   1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2] #USA
OPTTRACK = [   1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1] #USA

# OPTTRACK = [   1, 1, 1, 0, 1,
#             1, 1, 1, 1, 0, 1,
#             1, 1, 1, 1, 0, 1,
#             1, 1, 1, 1, 0, 1,
#             1, 1, 1, 1, 0, 1 ]          # middlefinger

class TrackLimit:
    def __init__(self, piece, lane, level=0):
        self.piece = piece
        self.lane = lane
        self.level = level
        self.variant = {}
        self.limit_type = None  # selfCurve or degraded
        self.max_speed = MAX_SPEED
        self.min_max_speed = 0
        self.depends_on_switch = -1
        pass


def degrade_limit(length, target):
    # return target+BRAKE_POWER*length
    speed = target
    prev_speed = speed
    prev_length = length
    # if target == 0:
    #     speed = 1
    # print "degrade", length, target
    while length > 0 and speed > 0:  # todo do we need to go negative?
        # speed = (sqrt(pow(magic_x_value, 2)+4*magic_c_value*speed)+magic_x_value)/(2*magic_c_value)
        # speed = (n_speed - magic_c_value)/magic_x_value
        # print speed
        prev_speed = speed
        prev_length = length
        speed = (speed - magic_c_value) / magic_x_value
        length -= speed
    if length < 0:
        # print "\t\t\t\t\t",prev_speed,speed,prev_length,length
        speed = prev_speed + (prev_length / speed) * (speed - prev_speed)
    return speed
    # speed*magic_x_value+magic_c_value,pos
    # speed,(pos+speed)
    # ...
    # target,length


def print_info(speed, self_limit, desired_speed, throttle, radius, car_angle, index):
    # print "%4d: speed: %f , self_limit: %f ,  throttle: %1.1f (%d)" % (tick, speed, self_limit, next_limit, throttle, radius)
    date = datetime.date.fromordinal(tick + 2).isoformat()
    # if radius == RADIUS_STRAIGHT_CONST:
    #     log.write("%s,%f,%f,%f,%f\n" % (date, speed, self_limit, next_limit, throttle))
    # else:
    if log :
        log.write("%s,%f,%f,%f,%f,%f,%d,%f\n" % (
        date, speed, self_limit, desired_speed, throttle * 10, abs(car_angle) / 2, radius / 10, float(index) / 10))


def get_power(desired_speed, prev_speed):
    return (desired_speed - prev_speed * (1 - TRACTION)) * ACCELL_DIVIDER


# def get_future_speed(current_speed, power, steps=RESPONSE_DISTANCE):
#     result = current_speed
#     ideal_acc = power/ACCELL_DIVIDER
#     for i in range(steps):
#         result += ideal_acc - result*ACCELL_DISSIPATION
#     return result

def printlimit(limit, i, j, to, addr):
    # print limit.variant
    if limit.variant != {}:
        # print "%d(%d) switch: %d"%(i, j, limit.depends_on_switch)
        addr += "%d(%d)" % (to, limit.depends_on_switch)
        for var in limit.variant:
            printlimit(limit.variant[var], i, j, var, addr)
    else:
        print "%d:%s max:%f " % (i, addr + "%d" % to, limit.max_speed)


def mprint(limits):
    for i in range(len(limits)):
        piece = limits[i]
        for j in range(len(piece)):
            limit = piece[j]
            printlimit(limit, i, j, j, "")


class NoobBot(object):
    state = STATE.NORMAL
    throttle_val = 0
    pieces = []
    limits = []
    lanes = []
    track_length = 0
    last_piece_index = 0
    last_peace_in_peace_dist = 0
    lap_num = 0
    prev_speed = 0
    speed = 0
    last_angle = 0
    lane = 1
    got_turbo = False
    turbo_used = False
    breaking = False
    first_lap = True

    power_converter = {"0.00": 4, "0.01": 4}
    test_power = 1
    deceleration_hist = 0

    def __init__(self, socket, name, key):
        self.ci_run = False
        self.prev_real_speed = 0
        self.used_limit = 5
        self.switches = {}
        self.socket = socket
        self.name = name
        self.key = key
        self.real_speed = 0
        self.car_num = 0
        self.first_switch_done = False
        self.phase = 0

    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data, "gameTick": tick}))

    def send(self, msg):
        self.socket.send(msg + "\n")

    def join(self):
        return self.msg("join", {
            "name": self.name,
            "key": self.key
            # "trackName": "keimola",
            # "trackName": "germany",
            # # "trackName": "usa",
            # # "trackName": "france",
            # "password": "schumi4ever",
            # "carCount": 1
        })
        # return self.msg("join", {"name": self.name,
        #                          "key": self.key})

    # def compute_current_speed(self):
    #     self.speed = self.throttle_val / self.motor_power() + self.prev_real_speed * magic_x_value - magic_c_value    # get future speed

    def throttle(self, throttle):
        self.throttle_val = throttle
        self.msg("throttle", throttle)

    def ping(self):
        self.msg("ping", {})

    def run(self):
        self.join()
        self.msg_loop()

    def on_join(self, data):
        print("Joined")
        self.ping()

    def on_game_start(self, data):
        print("Race started")
        # if(OPTTRACK[0]!=0):                 # TODO ORLY?
        #     self.msg("switchLane", "Right")
        # else:
        self.ping()

    def get_speed(self, index, position):
        if index == self.last_piece_index:
            speed = position - self.last_peace_in_peace_dist
            self.last_peace_in_peace_dist = position
        else:
            speed = position + self.get_length(self.last_piece_index, self.lane) - self.last_peace_in_peace_dist
            self.last_peace_in_peace_dist = position
            self.last_piece_index = index
            if self.is_switch(index) and self.first_switch_done:
                if len(OPTTRACK)>1:
                    OPTTRACK.pop(0)
                if OPTTRACK[0]>self.lane:
                    self.msg("switchLane", "Right")
                elif OPTTRACK[0]<self.lane:
                    self.msg("switchLane", "Left")
        return speed

    def get_speed_limit(self, car_pos_index, new_pos, from_lane, dirs):
        # new_pos = car_pos + speed
        if len(dirs) < 2:
            print "No addres for limit!!!"  # %d:%d"%(car_pos_index,lane)
            # return limit.max_speed
            dirs = [self.lane, self.lane]
        if new_pos > self.get_length(car_pos_index, from_lane):
            new_pos = new_pos - self.get_length(car_pos_index, from_lane)
            car_pos_index = (car_pos_index + 1) % self.track_length  # TODO SWITCHES ?done?
            if self.is_switch(car_pos_index):
                dirs = dirs[1:-1]
        if len(dirs) < 2:
            print "No addres for limit!!!"  # %d:%d"%(car_pos_index,lane)
            # return limit.max_speed
            dirs = [self.lane, self.lane]
        car_pos_index_next = (car_pos_index + 1) % self.track_length
        self_limit = self.get_limit(self.limits[car_pos_index][from_lane], dirs)  # todo for selflimit shouldn't basic max(curve) be enought?
        if self.is_switch(car_pos_index):
            # print dirs, self.limits[car_pos_index_next]
            next_limit_value = self.get_limit(self.limits[car_pos_index_next][dirs[0]], dirs[1:-1])
        else:
            next_limit_value = self.get_limit(self.limits[car_pos_index_next][from_lane], dirs)
        #     next_limit = degrade_limit(self.get_length(car_pos_index, from_lane) - new_pos,
        #                            self.limits[car_pos_index][from_lane].max_speed)
        # print self.get_length(car_pos_index, from_lane) , new_pos
        next_limit = degrade_limit(self.get_length(car_pos_index, from_lane) - new_pos, next_limit_value)
        # print self_limit,next_limit
        # print "%f\t%f\t%f\t%f"%(self.speed,min(self_limit, next_limit),car_pos,new_pos)
        # print min(self_limit, next_limit),self_limit, next_limit
        return min(self_limit, next_limit)

    def get_desired_speed(self, car_pos_index, car_pos, from_lane, dirs=[0, 0, 0, 0, 0, 0, 0, 0]):
        # car_pos_index_next = (car_pos_index + 1) % self.track_length
        speed = self.speed
        self_limit = self.get_speed_limit(car_pos_index, car_pos, from_lane, dirs)
        future_limit = self.get_speed_limit(car_pos_index, car_pos + speed, from_lane, dirs)
        return min(self_limit, future_limit)
        # x2 = car_pos + speed
        # s2 = speed
        # speed_change = speed / 2
        # while speed_change > 0.001:
        #     lim_x3 = self.get_speed_limit(car_pos_index, x2 + s2, from_lane, dirs)
        #     print s2, lim_x3
        #     lim_x2 = min(
        #         degrade_limit(s2, lim_x3),
        #         self.get_speed_limit(car_pos_index, x2, from_lane, dirs)
        #     )
        #     if s2 == lim_x2:
        #         break
        #     elif s2 < lim_x2:
        #         s2 += speed_change
        #     elif s2 > lim_x2:
        #         s2 -= speed_change
        #     speed_change /= 2
        #     # print s2
        # return s2

    def get_limit(self, limit, addr):
        if len(addr) < 2:
            print "No addres for limit!!!"  # %d:%d"%(car_pos_index,lane)
            # return limit.max_speed
            addr = [self.lane, self.lane]
        if addr[0] in limit.variant:
            return self.get_limit(limit.variant[addr[0]], addr[1:-1])
        else:
            return limit.max_speed

    def get_desired_throttle(self, desired_speed):
        last = 1
        tries = 0
        while True:
            new_val = max(0, min(1, (desired_speed - self.speed * magic_x_value + magic_c_value) * self.motor_power(last)))
            if new_val == last or tries > 30:
                break
            last = new_val
            tries += 1
        return last

    def on_car_positions(self, data):
        global magic_x_value
        global magic_c_value
        global MOTOR_POWER

        for i in range(len(data)):
            car = data[i]
            if car['id']['name'] == self.name:
                self.car_num = i
                break

        car_angle = data[self.car_num]['angle']

        car_pos = data[self.car_num]['piecePosition']['inPieceDistance']
        car_pos_index = data[self.car_num]['piecePosition']['pieceIndex']
        self.lane = data[self.car_num]['piecePosition']['lane']['endLaneIndex']
        car_pos_index_next = (car_pos_index + 1) % self.track_length
        # car_pos_index_next_pp = (car_pos_index + 2) % self.track_length

        self.prev_real_speed = self.real_speed
        self.real_speed = self.get_speed(car_pos_index, car_pos)
        self.prev_speed = self.speed
        # self.compute_current_speed()
        self.speed = self.real_speed

        if not self.first_switch_done and car_pos > 5 and car_pos_index == 0:
            # OPTTRACK.pop(0)
            print OPTTRACK[0], self.lane
            if OPTTRACK[0] > self.lane:
                self.msg("switchLane", "Right")
            elif OPTTRACK[0]<self.lane:
                self.msg("switchLane", "Left")
            self.first_switch_done = True
            return
        # if self.speed != 0:
        #     speed_control = self.real_speed / self.speed
        # else:
        #     speed_control = self.real_speedrr / 0.00001  # bleh
        # print "%3d:%3d   speed check: %4.4f\r" % (car_pos_index, car_pos, speed_control * 100)
        print "%3d:%3d   speed:%f \r" % (car_pos_index, car_pos, self.speed),
        radius = self.get_radius(car_pos_index, self.lane)
        # print self.real_speed, "\t", car_pos, "\t", self.throttle_val
        direction = self.get_curve_direction(car_pos_index)
        direction_next = self.get_curve_direction(car_pos_index_next)
        angular_speed = (car_angle - self.last_angle)
        if direction != 0:
            angular_speed *= direction
        self.last_angle = car_angle

        if self.prev_real_speed == 0:
                self.prev_real_speed = 0.00000000001   # TODO YEAHHHHHH

        if self.phase == 5:  # normal run
            desired_speed = self.get_desired_speed(car_pos_index, car_pos, self.lane, OPTTRACK)
            # desired_speed_l1 = self.get_desired_speed(car_pos_index, car_pos, 1,[1,1,1,1,1,1,1,1])
            limit = desired_speed
            if self.lap_num == 2 and car_pos_index + 3 > len(
                    self.pieces):  # finish boost (we don't care if we crash after)
                # print "finish boost"
                desired_speed = 100
            if car_angle * direction < CURVE_BOOST_LIMIT and angular_speed < ANGULAR_BOOST_BOUND and self.speed < 10 and abs(car_angle) < CURVE_BOOST_BOUND and abs(radius)>60:  # booost if car not angled
                cboost = desired_speed * CURVE_BOOST  # *(abs(car_angle)/CURVE_BOOST_LIMIT)
                if VERBOUSE_FUZZOID:
                    print "c-boost", cboost
                desired_speed += cboost
            elif angular_speed < ANGULAR_BOOST_LIMIT and abs(car_angle) < CURVE_BOOST_BOUND:  # boost if car straighting tu curve
                caboost = desired_speed * CURVE_BOOST  # *(abs(angular_speed-ANGULAR_BOOST_LIMIT)/ANGULAR_MAX)
                if VERBOUSE_FUZZOID:
                    print "c-ang-boost", caboost
                desired_speed += caboost
            elif car_angle * direction > CURVE_BRAKE_LIMIT and \
                            angular_speed > ANGULAR_BRAKE_LIMIT and \
                            direction != -1 * direction_next:  # curve counter brake
                angular_brake = desired_speed * CURVE_BRAKE * (abs(angular_speed) / ANGULAR_BRAKE_LIMIT)
                angle_brake = desired_speed * CURVE_BRAKE * (abs(car_angle) / CURVE_BRAKE_LIMIT)
                if VERBOUSE_FUZZOID:
                    print "c-brake", angular_brake, angle_brake
                desired_speed -= max(angular_brake, angular_brake)
            elif abs(car_angle)+angular_speed > EMERGENCY_BRAKE_BOUND or (radius == 0 and abs(car_angle)+abs(angular_speed) > EMERGENCY_BRAKE_BOUND):  # emergency brake
                angular_brake = desired_speed * CURVE_BRAKE * (abs(angular_speed) / ANGULAR_BRAKE_LIMIT)
                angle_brake = desired_speed * CURVE_BRAKE * (abs(car_angle) / CURVE_BRAKE_LIMIT)
                if VERBOUSE_FUZZOID:
                    print "e-brake", angular_brake, angle_brake
                desired_speed -= max(angular_brake, angular_brake)
            if USE_FUZZOID_POSTPROCERSSING:
                throttle = self.get_desired_throttle(desired_speed)
                self.used_limit = desired_speed
            else:
                throttle = self.get_desired_throttle(limit)
                self.used_limit = limit

            print "s:%2.6f ds:%2.6f t:%2.6f ca:%2.6f, av:%2.6f ,r:%+2.6f" % (
                self.speed, desired_speed, throttle, car_angle, angular_speed, radius * direction)
            if self.got_turbo and limit > 12 and radius == 0:
                self.got_turbo = False
                # self.turbo_used = True
                self.msg("turbo", "Engage!")
                print "Turbooooooo"
            else:
                self.throttle(throttle)
            print_info(self.speed, limit, desired_speed, throttle, radius, car_angle, car_pos_index)
            # print_info(self.speed, limit, self.real_speed, throttle, radius, car_angle, car_pos_index)
        elif self.phase == 1:  # looking for params
            if self.real_speed > 4.0:  # first lets get speed
                self.phase = 2  # now brake
                print "Phase 2"
                self.throttle(BRAKE_POWER)
            else:
                self.throttle(CONS_MAX_THROTTLE)
        elif self.phase == 2:  # get 1. part of friction
            self.deceleration_hist = self.real_speed / self.prev_real_speed
            # print self.real_speed,"\t", self.real_speed/self.prev_real_speed
            print "Phase 3"
            # print self.deceleration_hist
            self.throttle(BRAKE_POWER)
            self.phase = 3
        elif self.phase == 3:  # get 2. part of friction
            deceleration = self.real_speed / self.prev_real_speed
            magic_x_value = (deceleration * self.real_speed - self.deceleration_hist * self.prev_real_speed) / ( self.real_speed - self.prev_real_speed)
            magic_c_value = (deceleration - self.deceleration_hist) / (1 / self.real_speed - 1 / self.prev_real_speed)
            # print self.real_speed, magic_x_value, magic_c_value
            self.deceleration_hist = deceleration
            self.phase = 4
            print "Phase 4"
            self.throttle(self.test_power)
        elif self.phase == 4:  # get motor power
            # print self.real_speed, self.prev_real_speed
            MOTOR_POWER = self.test_power / (self.real_speed - self.prev_real_speed * magic_x_value + magic_c_value)
            # self.power_converter["%0.2f"%self.test_power] = MOTOR_POWER
            # print "Motor power:\t", MOTOR_POWER,"\t", self.real_speed,"\t", "%0.2f"%self.test_power
            # self.test_power -= 0.01
            # if self.test_power < 0.01:
            self.phase = 5
            self.set_speed_limits()
            # self.throttle(self.test_power)
            self.throttle(CONS_MAX_THROTTLE)
        else:
            self.phase = 1

    def motor_power(self, throttle=throttle_val):
        return MOTOR_POWER  # self.power_converter["%0.2f"%throttle]

    def is_curve(self, index):
        return 'angle' in self.pieces[index]

    def is_switch(self, index):
        return 'switch' in self.pieces[index]

    def on_crash(self, data):
        print("Someone crashed")
        if KILL_ON_CRASH:
            if log :
                log.close()
            exit()
        self.ping()

    def on_game_end(self, data):
        print("Race ended")
        if log :
            log.close()
        self.ping()

    def lap(self, data):
        print("lap time: %d (speed: %f)" % (data['lapTime']['millis'], CONS_MAX_THROTTLE))
        if self.first_lap:
            self.first_lap = False
            # self.set_speed_limits()
        self.lap_num += 1
        # self.random_throttle()
        self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()

    def on_game_init(self, data):
        global OPTTRACK
        track = data['race']['track']
        print "Track: %s (%d pieces, %d lanes)" % (track['name'], len(track['pieces']), len(track['lanes']))
        self.pieces = track['pieces']
        self.lanes = track['lanes']
        cars = data['race']['cars']
        self.track_info = TrackInfo.TrackInfo(track)
        self.first_switch_done = False
        race_session = data['race']['raceSession']
        if 'laps' in race_session:
            OPTTRACK = []
            for i in range(race_session['laps']+1):
                OPTTRACK += self.track_info.getLapSwitchesOptimal()
            print OPTTRACK
        for i in range(len(cars)):
            car = cars[i]
            if car['id']['name'] == self.name:
                self.car_num = i
                car
                break

        if 'durationMs' in race_session:
            self.ci_run = True
            self.phase = 0
            self.set_speed_limits()
            self.track_length = len(track['pieces'])
        elif not self.ci_run:
            self.phase = 0
            self.set_speed_limits()
            self.track_length = len(track['pieces'])
        else:
            print "something unexpected"
            print race_session

    def set_speed_limits(self):  # set limits for beginning of pieces
        track_len = len(self.pieces)
        self.limits = []
        for i in range(len(self.pieces)):
            self.limits.append([])
            for j in range(len(self.lanes)):
                lane_limits = TrackLimit(i, j)
                lane_limits.max_speed = MAX_SPEED

                if self.is_curve(i):
                    curve_limit = self.get_curve_speed_limit(i, j)
                    lane_limits.max_speed = curve_limit
                    lane_limits.min_speed = curve_limit
                    lane_limits.limit_type = CURVE
                if self.is_switch(i):
                    # prev = (i+len(self.pieces)) % len(self.pieces)
                    lane_limits.depends_on_switch = i

                    transf_limits = TrackLimit(i, j, 1)
                    transf_limits.max_speed = lane_limits.max_speed
                    # transf_limits.depends_on_switch = prev
                    lane_limits.variant[j] = transf_limits
                    if j > 0:
                        transf_limits = TrackLimit(i, j, 1)
                        transf_limits.max_speed = lane_limits.max_speed
                        # transf_limits.depends_on_switch = prev
                        lane_limits.variant[j - 1] = transf_limits
                    if j < len(self.lanes) - 1:
                        transf_limits = TrackLimit(i, j, 1)
                        transf_limits.max_speed = lane_limits.max_speed
                        # transf_limits.depends_on_switch = prev
                        lane_limits.variant[j + 1] = transf_limits
                self.limits[i].append(lane_limits)

        # for j in range(len(self.lanes)):
        #     self.limits[0][j] = TrackLimit()

        print ""
        print "Init:"
        print "Magic C:", magic_c_value
        print "Magic X:", magic_x_value
        print "Motor power:", MOTOR_POWER
        # print "Acceleration d.:", ACCELL_DISSIPATION

        i = 0
        cycles = 0
        prev_cycle = 0
        are_we_there_yet = True
        while True:  # while all limits are not set
            # mprint(self.limits)
            j = i
            i = (i - 1) % track_len  # cycle throughout pieces
            if i > j:
                cycles += 1
            piece = self.limits[i]
            # lanes_done = 0
            for lane in range(len(self.lanes)):  # and lanes
                # print i,j
                lane_limits = piece[lane]

                # if not lane_limits or lane_limits == []:                                     # init of new piece-lane

                piece_len = self.get_length(i, lane)
                # print lane_limits

                if self.is_switch(i):
                    new_max = 0
                    for var in lane_limits.variant:
                        t_are_we_there_yet, t_new_max = self.construct_limits(lane_limits.variant[var],
                                                                              self.limits[j][var], piece_len)
                        are_we_there_yet &= t_are_we_there_yet
                        new_max = max(new_max, t_new_max)
                else:
                    t_are_we_there_yet, new_max = self.construct_limits(lane_limits, self.limits[j][lane], piece_len)
                    are_we_there_yet &= t_are_we_there_yet

                # print new_max
                lane_limits.max_speed = new_max
                # print lane_limits.variant
                # printlimit(lane_limits, i, lane)

            # print are_we_there_yet,t_are_we_there_yet
            if prev_cycle != cycles:
                # mprint(self.limits)
                prev_cycle = cycles
                if (are_we_there_yet and cycles > 2) or cycles > 50:  # todo 50? that is bold
                    break
                are_we_there_yet = True

        print "In %d cycles constructed limits" % cycles
        mprint(self.limits)

    def construct_limits(self, limit, prev_limit, piece_len):
        # print "deg from"
        # printlimit(prev_limit, prev_limit.piece, prev_limit.lane)
        # print "to"
        # printlimit(limit, limit.piece, limit.lane)
        if prev_limit.variant != {}:  # there are multiple variants
            limit.depends_on_switch = prev_limit.depends_on_switch
            min_max = limit.max_speed
            max_max = 0
            is_there_something = False
            for var in prev_limit.variant:
                fresh = False
                if var not in limit.variant:
                    limit.variant[var] = TrackLimit(limit.piece, limit.lane, limit.level + 1)
                    fresh = True
                are_we_there_yet, var_max = self.construct_limits(limit.variant[var], prev_limit.variant[var],
                                                                  piece_len)
                if var_max > limit.max_speed:  # we are limited by something else more (probably curve)
                    self.remove_higher_limits(limit.variant, var, limit.max_speed)
                    are_we_there_yet = fresh
                else:
                    is_there_something = True
                    max_max = max(max_max, var_max)
                    min_max = min(min_max, var_max)
            if max_max == min_max:  # all limits same
                limit.variant = {}
            if is_there_something:
                limit.max_speed = max_max
                limit.min_max_speed = min_max
            # print "xxx",are_we_there_yet
            return are_we_there_yet, limit.max_speed
        else:
            degraded_prev_max_speed = degrade_limit(piece_len, prev_limit.max_speed)
            # print degraded_prev_max_speed
            are_we_there_yet = True
            # print are_we_there_yet, degraded_prev_max_speed, limit.max_speed
            if degraded_prev_max_speed < limit.max_speed:
                limit.max_speed = degraded_prev_max_speed
                limit.min_max_speed = degraded_prev_max_speed
                limit.limit_type = DEGRADED
                are_we_there_yet = False
            # print degraded_prev_max_speed, limit.max_speed
            # print "__",are_we_there_yet
            return are_we_there_yet, limit.max_speed

    def remove_higher_limits(self, variants, var, max_speed):
        if variants[var].max_speed > max_speed:
            # print "del limit",variants[var].max_speed,max_speed
            del variants[var]
        else:
            if variants[var].variant != {}:
                limit = variants[var]
                for var in limit.variant:
                    self.remove_higher_limits(limit.variant[var], limit.max_speed)

    def get_curve_speed_limit(self, track_piece, lane=1):
        radius = self.get_radius(track_piece, lane)
        limit = pow(float(radius) / LOW_CURVE_RAD, ECLIPSE_INDEX) * LOW_SPEED
        if radius in CURVE_TESTING:
            limit = CURVE_TESTING[radius]
        else:
            print "automated limit", radius, limit
        return limit

    def on_turbo(self, data):
        print "got turbo", data['turboDurationMilliseconds'], data['turboDurationTicks'], data['turboFactor']
        self.got_turbo = True

    def msg_loop(self):
        global tick
        msg_map = {
            'join': self.on_join,
            'joinRace': self.on_join,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
            'lapFinished': self.lap,
            'gameInit': self.on_game_init,
            'turboAvailable': self.on_turbo
        }
        socket_file = s.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            if 'gameTick' in msg:
                tick = msg['gameTick']
            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                print("Got {0}".format(msg_type))
                self.ping()
            line = socket_file.readline()

    def get_length(self, track_piece, lane=1):
        piece = self.pieces[track_piece]

        if "length" in piece:
            return piece["length"]

        angle = piece["angle"]

        radius = self.get_radius(track_piece, lane)

        # celkovy obvod
        circum = 2 * pi * radius

        # jedeme jenom cast bloku
        length = circum / 360.0 * abs(angle)

        return length

    def get_radius(self, track_piece, lane=1):
        piece = self.pieces[track_piece]
        if not 'angle' in piece:
            return RADIUS_STRAIGHT_CONST  # ale fuj
        angle = piece["angle"]

        if angle > 0:
            radius = piece["radius"] - self.lanes[lane]["distanceFromCenter"]
        else:
            radius = piece["radius"] + self.lanes[lane]["distanceFromCenter"]
        return radius

    def get_curve_direction(self, track_piece):
        piece = self.pieces[track_piece]
        if not 'angle' in piece:
            return 0
        angle = piece["angle"]
        return 1 if angle > 0 else -1


if __name__ == "__main__":
    print sys.argv
    print "bot ver 10:31d"
    if len(sys.argv) != 5:
        #print("Usage: ./run host port botname botkey")
        host, port, name, key = "testserver.helloworldopen.com", "8091", "WileECoyote", "drf7s9f321o/Gg"
        # host, port, name, key = "172.31.30.10", "10115", "I. C. Wiener", "drf7s9f321o/Gg"
    else:
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((host, int(port)))
    bot = NoobBot(s, name, key)
    bot.run()


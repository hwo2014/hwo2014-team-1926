# -*- coding: utf-8 -*-

from math import pi

class TrackInfo(object):
    def __init__(self, data):
        self.name = data["id"]
        self.pieces = data["pieces"]
        # todo: lanes maji predavany explicitne index, jsou vzdycky serazene?
        self.lanes = data["lanes"]


    def getLapLength(self, toPiece = None, lane = 0):
        dist = 0

        if toPiece == None:
            toPiece = len(self.pieces)

        for i in range(toPiece, 0, -1):
            dist += self.getLength(i-1, lane)

        return dist

    def getLapLengthOptimal(self, toPiece = None):
        """Vrati delku kola pokud se jede po nejoptimalnejsich drahach"""
        dist = 0
        lane = 0

        if toPiece == None:
            toPiece = len(self.pieces)

        for i in range(toPiece,0, -1):
            if self.isSwitchable(i):
                lane = self.getOptimalLane(i)
            dist += self.getLength(i-1, lane)

        return dist

    def getLapSwitchesOptimal(self):
        """Vrati poradi switchu"""
        lane = 0
        lanes = []

        for i in range(0, len(self.pieces)):
            if self.isSwitchable(i):
                lane = self.getOptimalLane(i)
                lanes.append(lane)

        return lanes

    def getLength(self, trackPiece, lane):
        """Vrati delku daneho dilku"""
        piece = self.getPiece(trackPiece)

        # pokud ma delku, je rovnej a muzeme ho rovnou pouzit
        if "length" in piece:
            return piece["length"]

        angle = piece["angle"]

        # je zakulacenej
        # vypocitame polomer pro drahu

        if angle > 0:
            radius = piece["radius"] - self.lanes[lane]["distanceFromCenter"]
        else:
            radius = piece["radius"] + self.lanes[lane]["distanceFromCenter"]

        # celkovy obvod
        circum = 2*pi*radius

        # jedeme jenom cast bloku
        length = circum * (abs(angle)/360.0)

        return length

    def getPiece(self, index):
        """Vrati dil trati dany indexem, zalamuje index"""
        index = index % len(self.pieces)
        return self.pieces[index]

    def isSwitchable(self, index):
        """Vrati True pokud dany dilek umoznuje prepnuti"""
        # pouzijeme metodu, portoze osetruje preteceni
        piece = self.getPiece(index)
        return piece["switch"] if "switch" in piece else False

    def getNextSwitch(self, fromIndex):
        """
        Zkusi najit nejblizsi dil ktery umozni prepnuti.
        Hledani zacne na fromIndex vcetne.
        """
        count = len(self.pieces)
        for i in xrange(0,count):
            index = (fromIndex+i) % count
            piece = self.pieces[index]
            if "switch" in piece and piece["switch"]:
                return index
        # zadny dil neexistuje
        return None

    def getOptimalLane(self, fromIndex):
        """
            Vrati index nejkratsi drahy pro nejblizsi usek.
        """
        # zmenit drahu muzeme az na dalsi vyhybce
        nextSwitch = self.getNextSwitch(fromIndex)
        # a zajima nas delka az na dalsi vyhybku
        optimizeUntil = self.getNextSwitch(nextSwitch+1)

        # slovnik pro vzdalenosti drah
        distances = {lane["index"]:0 for lane in self.lanes}

        count = len(self.pieces)
        index = nextSwitch
        while index != optimizeUntil:
            # pricteme delku pro kazdou drahu zvlast
            for lane in self.lanes:
                distances[lane["index"]] += self.getLength(index, lane["index"])
            index = (index+1) % count

        # vratime drahu s minimalni delkou
        return min(distances, key = distances.get)





